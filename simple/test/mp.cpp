#include <stdio.h>
#include <string.h>
#include <mpi.h>
int main(int argc, char*argv[]) {
  int name, p, source, dest, tag = 0;
  char message[100];
  MPI_Status status;
  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&name);
  MPI_Comm_size(MPI_COMM_WORLD,&p);
  if (name != 0) {
    printf("Processor %d of %d\n",name, p);
    sprintf(message,"greetings from process %d!",name);
    dest = 0;
    MPI_Send(message, strlen(message)+1,MPI_CHAR, dest, tag,MPI_COMM_WORLD);
  } else {
    printf("processor 0, p = %d ",p);
    for(source=1; source < p; source++) {
      MPI_Recv(message,100, MPI_CHAR, source,tag, MPI_COMM_WORLD, &status);
      printf("%s\n",message);
    }
  }
  MPI_Finalize();
}

