#include <iostream>
#include <iomanip>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

#define SWARM_SIZE 25
#define MAX_X 1.0
#define MIN_X -1.0
#define MAX_Y 1.0
#define MIN_Y -1.0

#define ITERS 100000000

#define w 0.8
#define Fg 0.1
#define Fp 0.1

using namespace std;




struct particula {
  float* posicion;
  float* mejor_posicion;
  float* velocidad;
//  float x, y;     // position
//  float px, py;   // best position
//  float vx, vy;   // speed
};


double fitness(int np, int ng, int na, int* asignaturas, float* posicion) {
  int max = 0;
  int min = np;
  int diferencias[na];
  int grupos[ng];
  for (int a = 0; a < na; a++) {
    max = 0;
    min = np;
    memset(grupos, 0, ng*sizeof(int));
    for (int p = 0; p < np; p++) {
      if (asignaturas[a*np + p] == 1) {
        grupos[(int)roundf(posicion[p])]++;
      }
    }
    for (int g = 0; g < ng; g++) {
      if (grupos[g] > max) {
        max = grupos[g];
      }
      if (grupos[g] < min) {
        min = grupos[g];
      }
    }
    diferencias[a] = max - min;
    for (int i = 0; i < ng; i++) {
      cout << grupos[i] << " ";
    }
    cout << endl;
  }
  cout << "->" << endl;
  max = 0;
  min = np;
  for (int a = 0; a < na; a++) {
    if (diferencias[a] > max) {
      max = diferencias[a];
    }
    if (diferencias[a] < min) {
      min = diferencias[a];
    }
    cout << diferencias[a] << " ";
  }
  cout << endl;
  
  
  

  return max - min;
}


void escribir_particula(struct particula* particula, int np) {
  for (int p = 0; p < np; p++) {
    cout << setw(6) << setfill(' ') << particula->posicion[p];
  }
  cout << endl;
}


void escribir_enjambre(struct particula enjambre[], int particulas, int np) {
  for (int p = 0; p < particulas; p++) {
    escribir_particula(&enjambre[p], np);
  }
}



int main() {
  
  int np, ng, na;
  int* asignaturas;
  
  cin >> np >> ng >> na;
  asignaturas = (int*)malloc(np*na*sizeof(int));
  for (int p = 0; p < np; p++) {
    for (int a = 0; a < na; a++) {
      cin >> asignaturas[a*np + p];
    }
  }
  
  
  struct particula pa;
  pa.posicion = (float*)malloc(np*sizeof(float));
  
  pa.posicion[0] = 0;
  pa.posicion[1] = 0;
  /*pa.posicion[2] = 0;
  pa.posicion[3] = 0;
  pa.posicion[4] = 0;
  pa.posicion[5] = 0;
  pa.posicion[6] = 0;
  pa.posicion[7] = 0;
  pa.posicion[8] = 0;*/
    
  cout << "fitness: " << fitness(np, ng, na, asignaturas, pa.posicion) << endl;


  
}










