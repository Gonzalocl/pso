#include <iostream>
#include <omp.h>



using namespace std;

struct uno {
  void* ptr;
};

void func(int i, int n) {
  #pragma omp critical
  cout << "i: " << i << ", n: " << n << endl;
}

int main() {
  omp_set_num_threads(100);
  #pragma omp parallel
  {
  func(omp_get_thread_num(), omp_get_num_threads());
  int s = 25;
  struct uno arr[s];
  for (int i = 0; i < s; i++) {
    arr[i].ptr = malloc(100);
  }
  cout << arr[100].ptr << endl;
  for (int i = 0; i < s; i++) {
    free(arr[i].ptr);
  }
  }


}


