#include <iostream>
#include <iomanip>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <omp.h>


#define SWARM_SIZE 25
/*#define MAX_X 1.0
#define MIN_X -1.0
#define MAX_Y 1.0
#define MIN_Y -1.0*/

#define ITERS 1000000

#define w 0.8
#define Fg 0.3
#define Fp 0.1

using namespace std;

////////////////////////
// Version secuencial //
////////////////////////
#ifdef SECUENCIAL
struct particula {
  float* posicion;
  float* mejor_posicion;
  float* velocidad;
};


double fitness(int np, int ng, int na, int* asignaturas, float* posicion) {
  int max = 0;
  int min = np;
  int diferencias[na];
  int grupos[ng];
  for (int a = 0; a < na; a++) {
    max = 0;
    min = np;
    memset(grupos, 0, ng*sizeof(int));
    for (int p = 0; p < np; p++) {
      if (asignaturas[a*np + p] == 1) {
        grupos[(int)roundf(posicion[p])]++;
      }
    }
    for (int g = 0; g < ng; g++) {
      if (grupos[g] > max) {
        max = grupos[g];
      }
      if (grupos[g] < min) {
        min = grupos[g];
      }
    }
    diferencias[a] = max - min;
  }
  max = 0;
  min = np;
  for (int a = 0; a < na; a++) {
    if (diferencias[a] > max) {
      max = diferencias[a];
    }
    if (diferencias[a] < min) {
      min = diferencias[a];
    }
  }

  return max - min;
}


void escribir_particula(struct particula* particula, int np) {
  cout << "(" << (int)roundf(particula->posicion[0]);
  for (int p = 1; p < np; p++) {
    cout << "," << setw(2) << setfill(' ') << (int)roundf(particula->posicion[p]);
  }
  cout << ")";
}


void escribir_enjambre(struct particula enjambre[], int particulas, int np) {
  for (int p = 0; p < particulas; p++) {
    escribir_particula(&enjambre[p], np);
    cout << endl;
  }
}

double secuencial(int np, int ng, int na, int *asignaturas,
                  int n_particulas, int iteraciones, long tiempo,
                  double fvelocidad, double fparticula, double fglobal) {

  struct particula enjambre[n_particulas];
  struct particula mejor_global; // mejor posicion del enjambre
  double mejor_fitness; // fitness de la mejor posicion del enjambre
  double fitness_posicion; // variable auxiliar
  
  // inicializar de forma aleatoria
  mejor_global.posicion = (float*)malloc(np*sizeof(float));
  for (int p = 0; p < np; p++) {
    mejor_global.posicion[p] = (float)rand() / RAND_MAX * (ng - 1);
  }
  mejor_fitness = fitness(np, ng, na, asignaturas, mejor_global.posicion);
  
  for (int i = 0; i < n_particulas; i++) {
    enjambre[i].posicion = (float*)malloc(np*sizeof(float));
    enjambre[i].mejor_posicion = (float*)malloc(np*sizeof(float));
    enjambre[i].velocidad = (float*)malloc(np*sizeof(float));
    for (int p = 0; p < np; p++) {
      enjambre[i].posicion[p] = (float)rand() / RAND_MAX * (ng - 1);
      enjambre[i].mejor_posicion[p] = enjambre[i].posicion[p];
      int positivo = rand() % 2 ? 1 : -1;
      enjambre[i].velocidad[p] = (float)rand() / RAND_MAX * (ng - 1) * positivo;
    }
    fitness_posicion = fitness(np, ng, na, asignaturas, enjambre[i].posicion);
    if (fitness_posicion < mejor_fitness) {
      memcpy(mejor_global.posicion, enjambre[i].posicion, np*sizeof(float));
      mejor_fitness = fitness_posicion;
    }
  }
  
  // condicion de parada
  int tiempo_tope = time(NULL) + tiempo;
  int tiempo_actual = time(NULL);
  int it = 0;
  while ( it < iteraciones && tiempo_actual < tiempo_tope && round(mejor_fitness) != 0 ) {
    for (int i = 0; i < n_particulas; i++) {
    
      for (int p = 0; p < np; p++) {
        
        // calcular nueva velocidad
        enjambre[i].velocidad[p] = fvelocidad*enjambre[i].velocidad[p] +
          fparticula*((float)rand() / RAND_MAX)*(enjambre[i].mejor_posicion[p] - enjambre[i].posicion[p]) +
          fglobal*((float)rand() / RAND_MAX)*(mejor_global.posicion[p] - enjambre[i].posicion[p]);
        
        // calcular nueva posicion
        enjambre[i].posicion[p] = enjambre[i].posicion[p] + enjambre[i].velocidad[p];
        
        // no salirse de los limites
        if (enjambre[i].posicion[p] < 0) {
          enjambre[i].posicion[p] = 0;
        }
        else if (enjambre[i].posicion[p] > (ng - 1)) {
          enjambre[i].posicion[p] = (ng - 1);
        }
        
      }
      
      // comprobar si se a encontrado un mejor posicion de la particula y global
      fitness_posicion = fitness(np, ng, na, asignaturas, enjambre[i].posicion);
      if (fitness_posicion < fitness(np, ng, na, asignaturas, enjambre[i].mejor_posicion)) {
        memcpy(enjambre[i].mejor_posicion, enjambre[i].posicion, np*sizeof(float));
        if (fitness_posicion < mejor_fitness) {
          memcpy(mejor_global.posicion, enjambre[i].posicion, np*sizeof(float));
          mejor_fitness = fitness_posicion;
        }
      }
    }
    
    cout << mejor_fitness << endl;
    tiempo_actual = time(NULL);
    it++;
  }
  
  // imprimir resultados y comprobar porque termino
  string condicion;
  if (it == iteraciones) {
    condicion = "Iteraciones";
  }
  else if (tiempo_actual >= tiempo_tope) {
    condicion = "Tiempo";
  }
  else {
    condicion = "Fitness";
  }
  cout << "Condicion de parada: " << condicion << endl;
  cout << "Tiempo:              " << (tiempo_actual-tiempo_tope+tiempo) << endl;
  cout << "Iteraciones:         " << it << endl;
  cout << "Fitness:             " << (int)round(mejor_fitness) << endl;
  cout << "Asignacion:          ";
  escribir_particula(&mejor_global, np);
  cout << endl;
  
  return mejor_fitness;
}
#endif


////////////////////////////////
// Version memoria compartida //
////////////////////////////////
#ifdef MEMORIACOMPARTIDA
struct particula {
  float* posicion;
  float* mejor_posicion;
  float* velocidad;
};


double fitness(int np, int ng, int na, int* asignaturas, float* posicion) {
  int max = 0;
  int min = np;
  int diferencias[na];
  int grupos[ng];
  for (int a = 0; a < na; a++) {
    max = 0;
    min = np;
    memset(grupos, 0, ng*sizeof(int));
    for (int p = 0; p < np; p++) {
      if (asignaturas[a*np + p] == 1) {
        grupos[(int)roundf(posicion[p])]++;
      }
    }
    for (int g = 0; g < ng; g++) {
      if (grupos[g] > max) {
        max = grupos[g];
      }
      if (grupos[g] < min) {
        min = grupos[g];
      }
    }
    diferencias[a] = max - min;
  }
  max = 0;
  min = np;
  for (int a = 0; a < na; a++) {
    if (diferencias[a] > max) {
      max = diferencias[a];
    }
    if (diferencias[a] < min) {
      min = diferencias[a];
    }
  }

  return max - min;
}


void escribir_particula(struct particula* particula, int np) {
  cout << "(" << (int)roundf(particula->posicion[0]);
  for (int p = 1; p < np; p++) {
    cout << "," << setw(2) << setfill(' ') << (int)roundf(particula->posicion[p]);
  }
  cout << ")";
}


void escribir_enjambre(struct particula enjambre[], int particulas, int np) {
  for (int p = 0; p < particulas; p++) {
    escribir_particula(&enjambre[p], np);
    cout << endl;
  }
}


//TODO probar ha hacer una copia en tmp con atomic 
double openmp(int np, int ng, int na, int *asignaturas,
                  int n_particulas, int iteraciones, long tiempo,
                  double fvelocidad, double fparticula, double fglobal,
                  int n_hilos) {


  // inicializar variables compartidas antes de crear los hilos
  struct particula mejor_global; // mejor posicion del enjambre
  double mejor_fitness; // fitness de la mejor posicion del enjambre
  mejor_global.posicion = (float*)malloc(np*sizeof(float));
  for (int p = 0; p < np; p++) {
    mejor_global.posicion[p] = (float)rand() / RAND_MAX * (ng - 1);
  }
  mejor_fitness = fitness(np, ng, na, asignaturas, mejor_global.posicion);
  omp_set_num_threads(n_hilos);
  #pragma omp parallel
  {
    // inicializar de forma aleatoria
    struct particula enjambre[n_particulas];
    double fitness_posicion; // variable auxiliar
    for (int i = 0; i < n_particulas; i++) {
      enjambre[i].posicion = (float*)malloc(np*sizeof(float));
      enjambre[i].mejor_posicion = (float*)malloc(np*sizeof(float));
      enjambre[i].velocidad = (float*)malloc(np*sizeof(float));
      for (int p = 0; p < np; p++) {
        enjambre[i].posicion[p] = (float)rand() / RAND_MAX * (ng - 1);
        enjambre[i].mejor_posicion[p] = enjambre[i].posicion[p];
        int positivo = rand() % 2 ? 1 : -1;
        enjambre[i].velocidad[p] = (float)rand() / RAND_MAX * (ng - 1) * positivo;
      }
      fitness_posicion = fitness(np, ng, na, asignaturas, enjambre[i].posicion);
      #pragma omp critical
      {
        if (fitness_posicion < mejor_fitness) {
          memcpy(mejor_global.posicion, enjambre[i].posicion, np*sizeof(float));
          mejor_fitness = fitness_posicion;
        }
      }
    }
    
    // condicion de parada
    int tiempo_tope = time(NULL) + tiempo;
    int tiempo_actual = time(NULL);
    int it = 0;
    
    bool no_optimo;
    #pragma omp critical
    {
      no_optimo = round(mejor_fitness) != 0;
    }
    //TODO rename
    struct particula placeholder;
    placeholder.posicion = (float*)malloc(np*sizeof(float));
    while ( it < iteraciones && tiempo_actual < tiempo_tope && no_optimo ) {
      for (int i = 0; i < n_particulas; i++) {
      
        #pragma omp critical
        {
          memcpy(placeholder.posicion, mejor_global.posicion, np*sizeof(float));
        }
        for (int p = 0; p < np; p++) {
          
          // calcular nueva velocidad
          enjambre[i].velocidad[p] = fvelocidad*enjambre[i].velocidad[p] +
            fparticula*((float)rand() / RAND_MAX)*(enjambre[i].mejor_posicion[p] - enjambre[i].posicion[p]) +
            fglobal*((float)rand() / RAND_MAX)*(placeholder.posicion[p] - enjambre[i].posicion[p]);
          
          // calcular nueva posicion
          enjambre[i].posicion[p] = enjambre[i].posicion[p] + enjambre[i].velocidad[p];
          
          // no salirse de los limites
          if (enjambre[i].posicion[p] < 0) {
            enjambre[i].posicion[p] = 0;
          }
          else if (enjambre[i].posicion[p] > (ng - 1)) {
            enjambre[i].posicion[p] = (ng - 1);
          }
          
        }
        
        // comprobar si se a encontrado un mejor posicion de la particula y global
        fitness_posicion = fitness(np, ng, na, asignaturas, enjambre[i].posicion);
        if (fitness_posicion < fitness(np, ng, na, asignaturas, enjambre[i].mejor_posicion)) {
          memcpy(enjambre[i].mejor_posicion, enjambre[i].posicion, np*sizeof(float));
          #pragma omp critical
          {
            if (fitness_posicion < mejor_fitness) {
              memcpy(mejor_global.posicion, enjambre[i].posicion, np*sizeof(float));
              mejor_fitness = fitness_posicion;
            }
          }
        }
      }
      
      tiempo_actual = time(NULL);
      it++;
    }
    // liberar memoria
    for (int i = 0; i < n_particulas; i++) {
      free(enjambre[i].posicion);
      free(enjambre[i].mejor_posicion);
      free(enjambre[i].velocidad);
    }
    free(placeholder.posicion);
    
    
    
  }

  //TODO imprimir resultados y comprobar porque termino
  /*string condicion;
  if (it == iteraciones) {
    condicion = "Iteraciones";
  }
  else if (tiempo_actual >= tiempo_tope) {
    condicion = "Tiempo";
  }
  else {
    condicion = "Fitness";
  }
  cout << "Condicion de parada: " << condicion << endl;
  cout << "Tiempo:              " << (tiempo_actual-tiempo_tope+tiempo) << endl;
  cout << "Iteraciones:         " << it << endl;*/
  cout << "Fitness:             " << (int)round(mejor_fitness) << endl;
  //TODO debug
  cout << "Check:               " << fitness(np, ng, na, asignaturas, mejor_global.posicion) << endl;
  cout << "Asignacion:          ";
  escribir_particula(&mejor_global, np);
  cout << endl;
  free(mejor_global.posicion);
  return mejor_fitness;
}
#endif











int main(int argc,char *argv[]) {
  
  int np, ng, na;
  int* asignaturas;
  
  cin >> np >> ng >> na;
  asignaturas = (int*)malloc(np*na*sizeof(int));
  for (int p = 0; p < np; p++) {
    for (int a = 0; a < na; a++) {
      cin >> asignaturas[a*np + p];
    }
  }
  

  cout << fixed << setprecision(2);
  srand(time(NULL));
  
  
  
#ifdef SECUENCIAL
  secuencial(np, ng, na, asignaturas, SWARM_SIZE, ITERS, 5, w, Fp, Fg);
#endif

#ifdef MEMORIACOMPARTIDA
  openmp(np, ng, na, asignaturas, SWARM_SIZE, ITERS, 5, w, Fp, Fg, 4);
#endif


  free(asignaturas);
  
}









//TODO liberar
